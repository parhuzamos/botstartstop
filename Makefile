NAME:=BotStartStop
SHELL:=/bin/bash
DATE:=$$(date +"%Y-%m-%d")
DATETIME:=$$(date +"%Y-%m-%d_%H-%M-%S")
DIR:=$$(dirname $(abspath $(lastword $(MAKEFILE_LIST))))

MODS_DIR=~/.local/share/Steam/steamapps/common/Autonauts/Autonauts_Data/StreamingAssets/Mods
TESTING=..
FILES=BotStartStop.lua
STEAM_MOD_ID=2739455777

.PHONY: help u d test generate
.DEFAULT_GOAL := help
help: ## Show this help
	@grep -E '^[a-zA-Z0-9_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sed 's/Makefile://g' | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

watch:		## watch for changed files and run
	@ls $(FILES) $(TESTING)/test.lua $(TESTING)/utils.lua | entr /bin/bash -c "export MOD=$(NAME) && cd $(TESTING) && make run"

dev:		## switch to development environment
	@source ~/bin/includes && \
		echo "Switch to development..." && \
		rm -rf $(MODS_DIR)/$(NAME) &>/dev/null && \
		ln -s $$(realpath ../$(NAME)) $(MODS_DIR)/ && \
		_succ "Done." || \
		_err "Error."
	

prod:		## switch to prod environment
	@source ~/bin/includes && \
		echo "Switch to production..." && \
		rm -rf $(MODS_DIR)/$(NAME) &>/dev/null && \
		mkdir -p $(MODS_DIR)/$(NAME)/ && \
		rsync -aL .release/source/ $(MODS_DIR)/$(NAME)/ && \
		_succ "Done." || \
		_err "Error."

check_debuglog:
	@source ~/bin/includes && \
		(grep --color=ALWAYS -n "\s.ModDebug" $(FILES) && _err "Remove debug logs." && exit 1 || _succ "Debuglog is OK.")

check_tag:
	@source ~/bin/includes && \
		version=$$(git tag -l 'v*' --format='%(refname:short)' | tail -n 1) && \
		commit=$$(git tag -l 'v*' --format='%(objectname:short)' | tail -n 1) && \
		curr_commit=$$(git rev-parse --short HEAD) && \
		echo "Latest version is $$(_yellow3 @$${version}), @$${commit}. Current commit is: $$(_gray2 @$${curr_commit})" && \
		[[ "$${commit}" == "$${curr_commit}" ]] && \
		_succ "Last commit is tagged with $$(_yellow3 @$${version})." || (_err "Missing tag on the last commit, use git-tag-last-commit." && exit 1)

check_changelog:
	@source ~/bin/includes && \
		version=$$(git tag -l 'v*' --format='%(refname:short)' | tail -n 1) && \
		remote_version=$$(git ls-remote --tags 2>&1 | tail -n 1 | awk '{print $$2}') && \
		echo "Changelog for $$(_lightyellow $${remote_version}) -> $$(_yellow3 @$${version}):" && \
		git log --format='	%ci %h %s' origin/master..$${version} && \
		git log --format='	%ci %h %s' origin/master..$${version} README.md | grep "$$(date +"%Y-%m-%d")" >/dev/null && \
		_succ "Changelog entry is OK." || (_err "Missing changelog entry" && exit 1)

check_title:
	@source ~/bin/includes && \
		version=$$(git tag -l 'v*' --format='%(refname:short)' | tail -n 1) && \
		head -n 1 README.md | grep "$${version}" &>/dev/null && \
		_succ "Title includes $$(_yellow3 @$${version})." || (_err "Missing or incorrect version in the title." && exit 1)

check_source_version:
	@source ~/bin/includes && \
		version=$$(git tag -l 'v*' --format='%(refname:short)' | tail -n 1) && \
		grep "ModBase.SetSteamWorkshopDetails" BotStartStop.lua | grep "$${version}" &>/dev/null && \
		_succ "Source includes $$(_yellow3 @$${version})." || (_err "Missing or incorrect version in the source." && exit 1)

check_dirty:
	@source ~/bin/includes && \
		git status | grep "nothing to commit" >/dev/null && \
		_succ "Git is clean" || (_err "Git is dirty." && exit 1)

check: check_debuglog	check_tag check_changelog check_title check_source_version check_dirty		## check if release is ready
	@source ~/bin/includes && \
		_succ "Check."

release_to_workshop:
	@source ~/bin/includes && \
		_boldyellow "Do the release process in the game and exit." && \
		pause "Press ENTER to continue after finishing the release in the game or Ctrl+C to abort..." && echo && \
		version=$$(git tag -l 'v*' --format='%(refname:short)' | tail -n 1) && \
		curl -s "https://steamcommunity.com/sharedfiles/filedetails/?id=$(STEAM_MOD_ID)" | grep "$${version}" | grep "<title>" &>/dev/null && \
		_succ "Steam Workshop release success." || (_err "Steam Workshop release failed." && exit 1)

test_in_game:
	@source ~/bin/includes && \
		rm -rf $(MODS_DIR)/$(NAME) &>/dev/null && \
		_boldyellow "Check the MOD in the game using Steam Workshop." && \
		pause "Press ENTER to continue after testing in the game or Ctrl+C to abort..." && echo && \
		_succ "Test success." || (_err "Test failed." && exit 1)

publish:
	@source ~/bin/includes && \
		git push origin master --tags && \
		_succ "Publication success." || (_err "Publication failed." && exit 1)

release: check prod release_to_workshop test_in_game publish dev	## do a release
	@source ~/bin/includes && \
		_succ "Done."


