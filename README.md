# BotStartStop v1.1.1

Automagically start and stop bots/scripts in [Autonauts](https://store.steampowered.com/app/979120/Autonauts/).

Steam Workshop: https://steamcommunity.com/sharedfiles/filedetails/?id=2739455777

![BotStartStop - bot/script start and stop](textures/BotStartStop.jpg)

When a bot is placed in the start-spot (and it's not learning), it's started automagically.  
Bots are stopped if waiting (default 4 seconds) in the stop-spot (see options). 
These stopped bots are moved from the red square to the yellow circle so the can be started again. 

A bot can put another bot in the start-spot or you can direct the output of a Bot Assembly Unit 
and after loading a 'Crude Data Storage' into the bot, it goes to do its job. 

Autostart of bots/scripts will be included in the upcoming release of Autonauts 
( see https://discord.com/channels/331886561044201473/333923003769290753/851846265981239306 )
so this is only a 'temporary workaround'.

https://gitlab.com/parhuzamos/botstartstop

## Changelog

### [v1.1.1] - 2022-02-06
Better development environment, prettier changelog

### [v1.1] - 2022-02-04 
Changed spots to to 2x1 tile, bots are not stopped immediately, a delay can be configured.

### [v1.0] - 2022-02-01
Initial version.
