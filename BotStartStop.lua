function SteamDetails()
    local desc = "When a bot is placed in the start-spot (and it's not learning), it's started automagically. " ..
					"Bots are stopped if waiting (default 4 seconds) in the stop-spot (see options). " ..
					"These stopped bots are moved from the red square to the yellow circle so the can be started again. \r\n" ..
					"\r\n" ..
					"A bot can put another bot in the start-spot or you can direct the output of a Bot Assembly Unit " ..
					"and after loading a 'Crude Data Storage' into the bot, it goes to do its job. \r\n" ..
					"\r\n" ..
					"Autostarting of bots/scripts will be included in the upcoming release of Autonauts " ..
					"( see https://discord.com/channels/331886561044201473/333923003769290753/851846265981239306 ), " ..
					"so this is only a 'temporary workaround'.\r\n" .. 
					"\r\n" ..
					"https://gitlab.com/parhuzamos/botstartstop"
    ModBase.SetSteamWorkshopDetails("BotStartStop - Bot/script start and stop (autostart) v1.1.1", desc, {"bot", "script", "start", "stop", "autostart", "autostop"}, "BotStartStop.jpg")
end

mapWidth = 0
mapHeight = 0
---Milliseconds to wait for a bot to be stopped.
---This value must be enough for the slowest bot to enter, pickup a bot and leave
timeToWaitForToStop = 4000

frame = 0
---How often should we check for bots in the area, higher number means less check
frameSkips = 50

function ChangeSecondsBeforeStop(value, name)
	timeToWaitForToStop = value * 1000
end

function ChangeFrameSkips(value, name)
	frameSkips = value
end

function Dummy() end

function Expose()
	local warning = "Warning: bots can't cross or leave \n"
					.. "if set too low!!!\n"
					.. "I'm okay with that."
	ModBase.ExposeVariable("Seconds before stopping a bot", 4, ChangeSecondsBeforeStop, 0, 10)
	ModBase.ExposeVariable(warning, true, Dummy)
	ModBase.ExposeVariable("Frames to skip", 50, ChangeFrameSkips, 1, 200)
end

function Creation()
	ModBuilding.CreateBuilding("Bot Start", {"Panel"}, {1}, "BotStart",	{0, 0},	{0, 0}, {}, true )
	ModBuilding.SetBuildingWalkable("Bot Start", true)

	
	ModBuilding.CreateBuilding("Bot Stop",  {"Panel"}, {1}, "BotStop",	{0, 0},	{0, 0}, {}, true )
	ModBuilding.SetBuildingWalkable("Bot Stop", true)

	-- ModCustom.UpdateModelParameters()

	-- ModDebug.Log("bot start stop")
end

function AfterLoad()
	mapWidth = ModTiles.GetTilesWide() - 1
	mapHeight = ModTiles.GetTilesHigh() - 1
end

--- Is the bot learning? Is the got running a script?
--- Returns true if we should start/stop the bot.
function checkBot(uid, startOrStop) 
	if ModBot.GetIsLearning(uid) then
		return false
	else
		if startOrStop then
			return not ModBot.GetIsRunningScript(uid)
		else
			return ModBot.GetIsRunningScript(uid)
		end
	end
end

function findSpots(type, startOrStop)
	local signs = {}
	signs = ModBuilding.GetBuildingUIDsOfType(type, 0,0, mapWidth, mapHeight)
	--ModDebug.Log("signs: ", type, ",", #signs)

	for _, uid in ipairs(signs) do
		-- ModDebug.Log("found sign: ", uid)
		local coord = ModObject.GetObjectTileCoord(uid)
		-- ModDebug.Log("coord: ", coord[1], ":", coord[2])
		local rotation = ModBuilding.GetRotation(uid)
		--- The tile next to the building tile according to the rotation
		local nextTile = {coord[1], coord[2]}
		if rotation == 1 then
			nextTile[2] = nextTile[2] - 1
		elseif rotation == 2 then
			nextTile[1] = nextTile[1] + 1
		elseif rotation == 3 then
			nextTile[2] = nextTile[2] + 1
		else
			nextTile[1] = nextTile[1] - 1
		end
		
		local bots = {}
		if startOrStop then
			-- Bots can not be dropped at the "Bot Start" tile, only next to it, check for both tiles
			bots = ModTiles.GetObjectUIDsOfType("Worker", math.min(coord[1], nextTile[1]), math.min(coord[2], nextTile[2]), math.max(nextTile[1], coord[1]), math.max(nextTile[2], coord[2]))
			--ModDebug.Log(os.clock(), " checking: ", coord[1], ":", coord[2], " ", nextTile[1], ":", nextTile[2], " bot count: ", #bots)
		else
			bots = ModTiles.GetObjectUIDsOfType("Worker", coord[1], coord[2], coord[1], coord[2])
		 	--ModDebug.Log(os.clock(), " checking: ", coord[1], ":", coord[2], " bot count: ", #bots)
		end

	 	--ModDebug.Log("bots: ", #bots)

		for _, botId in ipairs(bots) do
			if checkBot(botId, startOrStop) then
				if startOrStop then
					-- ModDebug.Log("start bot: ", uid)
					ModBot.StartScript(botId)
				else
					--ModDebug.Log(os.clock(), " bot checked ", botId, " ", stopInfosByBotId[botId], " => ", stopInfosByBotId[botId] == nil)
					-- are we stopping this bot already?
					if (stopInfosByBotId[botId] == nil) then
						--ModDebug.Log(os.clock(), " ***** queue bot for stop:  ", botId)
						local timerId = ModTimer.SetCallback(checkIfBotStillInPositionAndStopIt, timeToWaitForToStop, false)
						stopInfosByBotId[botId] = {
							timer = timerId,
							coord = coord,
							moveTo = nextTile
						}
						botIdByTimerId[timerId] = botId
					end
				end
			end
		end
	end
end

--- table: timer Id, coord where to bot should be, moveTo where it should move to
stopInfosByBotId = {}
botIdByTimerId = {}
botIdsToMove = {}

--- Checks if a bot is still at the same tile and stop it if so
function checkIfBotStillInPositionAndStopIt(delayed, timerId)
	local botId = botIdByTimerId[timerId]
	local stopInfo = stopInfosByBotId[botId]
	local botCoord = ModObject.GetObjectTileCoord(botId)
	local isInTheSamePosition = botCoord[1] == stopInfo.coord[1] and botCoord[2] == stopInfo.coord[2];
	--ModDebug.Log(os.clock(), " checkIfBotStillInPositionAndStopIt, botId: ", botId, " in position: ", isInTheSamePosition)

	-- No need for this info, we checked the bot
	stopInfosByBotId[botId] = nil

	if (isInTheSamePosition) then
		ModBot.StopScript(botId)
		botIdsToMove[botId] = botId
		--- The bot is moved the next tile to the stop spot,
		--- don't stack up a lot of bots in the stopping tile
		--- as they would be checked over and over again
		ModObject.StartMoveTo(botId, stopInfo.moveTo[1], stopInfo.moveTo[2], 10)
	end
	ModTimer.DestroyCallback(timerId)
end

function OnUpdate()
	frame = frame+1

	for _,botId in pairs(botIdsToMove) do
		if ModObject.UpdateMoveTo(botId) then
			botIdsToMove[botId] = nil
		end
	end

	if frame < frameSkips then return end
	frame = 0

    findSpots("Bot Start", true)
    findSpots("Bot Stop", false)
end

